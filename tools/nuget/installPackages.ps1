$nugetPackagesConfigPath = Join-Path $(Split-Path -parent $MyInvocation.MyCommand.Definition) "libs"
Push-Location $nugetPackagesConfigPath
& nuget.exe install
Pop-Location