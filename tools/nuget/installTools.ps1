$nugetPackagesConfigPath = Join-Path $(Split-Path -parent $MyInvocation.MyCommand.Definition) "tools"
Push-Location $nugetPackagesConfigPath
& nuget.exe install
Pop-Location

function ConfigureNunit($xml)
{
    if($xml.configuration -eq $null -or $xml.configuration.startup -eq $null) {return}
    if($xml.configuration.startup.HasAttribute("useLegacyV2RuntimeActivationPolicy"))
    {
        $xml.configuration.startup.RemoveAttribute("useLegacyV2RuntimeActivationPolicy")
    }
    
    if($xml.GetElementsByTagName("supportedRuntime").Count -gt 0)
    {
        $xml.GetElementsByTagName("supportedRuntime").Item(0).SetAttribute("version", "4.0")
    }
    else
    {
        $newEl = $xml.CreateElement("supportedRuntime")
        $newEl.SetAttribute("version", "4.0")
        $xml.configuration.startup.AppendChild($newEl)
    }
}

function ConfigureSupportedRuntimeAttribute($el)
{
    $el.SetAttribute("version", "4.0")
    return $el
}

$nunitConfigFile =Join-Path $(Split-Path -parent $MyInvocation.MyCommand.Definition) "../_packages/NUnit.Runners.2.6.2/tools/nunit.exe.config"
[xml]$nunitConfig = Get-Content $nunitConfigFile
ConfigureNunit($nunitConfig)
$nunitConfig.Save($nunitConfigFile)

$nunitConfigFile =Join-Path $(Split-Path -parent $MyInvocation.MyCommand.Definition) "../_packages/NUnit.Runners.2.6.2/tools/nunit-console.exe.config"
[xml]$nunitConfig = Get-Content $nunitConfigFile
ConfigureNunit($nunitConfig)
$nunitConfig.Save($nunitConfigFile)
