DerivedProperties {
    $slnDir = Resolve-Path(Join-Path $psake.build_script_dir "..\")
    $webSlnFile = Join-Path $slnDir "Sample.sln"

    $sourceDir = Join-Path $slnDir "src"
    $sqlDir = Join-Path $slnDir "sql"
    $everytimeScriptsDir = Join-Path $sqlDir "everytimeScripts"
    $buildOutputDir = Join-Path $slnDir "_buildOutput"
    $webOutputDir = Join-Path $slnDir "_webOutput"

    $nugetDownloadPath = $env:TEMP
    $nugetDownloadUrl = "https://dist.nuget.org/win-x86-commandline/v${nugetVersion}/NuGet.exe"
    $nugetExePath = "${Env:\TEMP}\nugetv${nugetVersion}\"
    $nugetExe = "${nugetExePath}nuget.exe"

    $nodeZipDownloadFile = "${Env:\TEMP}\nodev${nodeVersion}.zip"
    $nodeZipDownloadUrl = "http://nodejs.org/dist/v${nodeVersion}/node-v${nodeVersion}-win-x64.zip"
    $nodeDownloadPath = "${Env:\TEMP}"
    $nodeExePath = "${Env:\TEMP}\node-v${nodeVersion}-win-x64\"
    $nodeExe = "${Env:\TEMP}\node-v${nodeVersion}-win-x64\node.exe"

    $toolsDir = Join-Path $slnDir "tools"
    $nugetDir = Join-Path $toolsDir "nuget"
    $webProjectFile = Join-Path $sourceDir "SampleWeb/SampleWeb.csproj"
    $nugetPackagesDir = Join-Path $slnDir "_packages"
    $toolsNugetPackagesDir = Join-Path $toolsDir "_packages"
    $roundhouseExe = Join-Path $toolsNugetPackagesDir "roundhouse.0.8.6-alpha1-20130218/bin/rh.exe"
    $webPackageDir = Join-Path $slnDir "_webPackage"
    $webPackageFile = Join-Path $webPackageDir "$($(Get-Item $webProjectFile).BaseName).zip"
    $dbPackageDir = Join-Path $slnDir "_dbPackage"
    $publishProfilesDir = Join-Path (Get-Item $webProjectFile).DirectoryName "/Properties/PublishProfiles"
    $nodeModulesDir = Join-Path (Get-Item $webProjectFile).DirectoryName "/node_modules"
    $testResultsDir = Join-Path $slnDir "_testResults"
    $testResultFile = Join-Path $testResultsDir "TestResult.xml"
    $testOutputFile = Join-Path $testResultsDir "TestOutput.txt"
    $testErrorOutputFile = Join-Path $testResultsDir "TestErrors.txt"
    $testRunner = Join-Path $toolsNugetPackagesDir "NUnit.Runners.2.6.2/tools/nunit-console.exe"
    $dbPackageDir = Join-Path $slnDir "_dbPackage"
    $packageDir = Join-Path $slnDir "_package"
    $packageZip = "${slnDir}\${env}_package.zip"
    $VSToolsDir = Join-Path $toolsDir "VS_tools"
}
