Include (Resolve-Path './default.ps1').Path

task cleanWebPublish -depends writeAllProperties {
    Write-Host "Deleting web output directory - '$webOutputDir'" -foregroundcolor "green"
    Delete $webOutputDir
}

task initWebPublish -depends cleanWebPublish {
    Write-Host "Creating web output directory - '$webOutputDir'" -foregroundcolor "green"
    New-Item $webOutputDir -type directory
}

task publishWeb -depends initWebPublish {
    $envPublishFile = $publishProfilesDir +  "\${env}_Publish.pubxml"
    if(-Not(Test-Path $envPublishFile)) {
        $envPublishFile = $publishProfilesDir +  "\${env}.pubxml"
        if(-Not (Test-Path $envPublishFile)) {
            $envPublishFile = $env
        } else {
            $envPublishFile = "Publish"
        }
    } else {
        $envPublishFile = "${env}_Publish"
    }
    Write-Host "Using publish profile '${envPublishFile}'" -ForegroundColor Green
    $logParam = '/l:FileLogger,Microsoft.Build.Engine;logfile=Web_Publish_LOG.log'
    Exec { msbuild /nologo /v:$msbuildVerbosity /m:$msbuildCpuCount /p:BuildInParralel=$msbuildParralel /p:Configuration="$buildConfiguration" /p:IsDesktopBuild=false /p:PublishProfile=$envPublishFile /p:Password=$publishPassword /p:OutputPath=$webOutputDir /p:DeployOnBuild=True /p:VisualStudioVersion=11.0 /p:VSToolsPath="$VSToolsDir" $logParam $webProjectFile }
}

task publishDB {
}

task publish -depends test, publishDB, publishWeb {
}
