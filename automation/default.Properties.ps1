#############################################################
#Declare default values for the properties that are applicable (which in almost all cases should be all properties)
#These properties will be overridden by property values set in local and machine specific properties files
#############################################################
properties {
    $debug = $false # this applies to debugging the build script itself not for the project build configuration type
    $buildConfiguration = "Release" # this applies to the build configuration with which all the projects will be built
    $msbuildVerbosity = "Minimal"
    $msbuildCpuCount = [System.Environment]::ProcessorCount
    $msbuildParralel = $true
    $enableMinification = "false"
    $runInitDB = $true
    $publishUsername = "username"
    $publishPassword = "PASSWORD"
    $deployIisAppPath = "Default Web Site/utwrap"

    $gruntTasks = @("default")
    $gruntOptions = @{"verbose" = $false}
    
    $nodeVersion = "6.2.1"
    $nugetVersion = "3.4.4"

    # this applies to versioning the assemblies produced by the build
    $majorVersion = 1
    $minorVersion = 0
    $patchVersion = 0
    $buildVersion = 00000
    
    $dbSetupConnectionString = "Data Source=.\SQLEXPRESS;Initial Catalog=SAMPLE;Integrated Security=SSPI;"
    $dbAppConnectionString = "Data Source=.\SQLEXPRESS;Initial Catalog=SAMPLE;Integrated Security=SSPI;"

    $serviceAddress = "net.pipe://localhost/SampleService"
    
    $testAssemblies = @()
}
