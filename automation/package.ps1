Include (Resolve-Path './default.ps1').Path

task cleanWebPackage -depends writeAllProperties {
    Write-Host "Deleting web output directory - '$webOutputDir'" -foregroundcolor "green"
    Delete $webOutputDir
    Write-Host "Deleting web package directory - '$webPackageDir'" -foregroundcolor "green"
    Delete $webPackageDir
}

task initWebPackage -depends cleanWebPackage {
    Write-Host "Creating web output directory - '$webOutputDir'" -foregroundcolor "green"
    New-Item $webOutputDir -type directory
    Write-Host "Creating web package directory - '$webPackageDir'" -foregroundcolor "green"
    New-Item $webPackageDir -type directory
}

task packageWeb -depends initWebPackage {
    $envPackageFile = $publishProfilesDir +  "\${env}_Package.pubxml"
    if(-Not(Test-Path $envPackageFile)) {
        $envPackageFile = $publishProfilesDir +  "\${env}.pubxml"
        if(-Not (Test-Path $envPackageFile)) {
            $envPackageFile = $env
        } else {
            $envPackageFile = "Package"
        }
    } else {
        $envPackageFile = "${env}_Package"
    }
    Write-Host "Using package profile '${envPackageFile}'" -ForegroundColor Green
    $logParam = '/l:FileLogger,Microsoft.Build.Engine;logfile=Web_Package_LOG.log'
    Exec { msbuild /nologo /v:$msbuildVerbosity /m:$msbuildCpuCount /p:BuildInParralel=$msbuildParralel /p:Configuration="$buildConfiguration" /t:Package /p:IsDesktopBuild=false /p:PublishProfile=${envPackageFile} /p:OutputPath=$webOutputDir /p:CreatePackageOnPublish=True /p:DeployOnBuild=False /p:MSDeployPublishMethod=Package /p:PackageLocation=$webPackageFile /p:VSToolsPath="$VSToolsDir" $logParam $webProjectFile }
}

task cleanDBPackage -depends writeAllProperties {
    Write-Host "Deleting DB package directory - '$dbPackageDir'" -foregroundcolor "green"
    Delete $dbPackageDir
}

task initDBPackage -depends cleanDBPackage {
    Write-Host "Creating DB output directory - '$dbPackageDir'" -foregroundcolor "green"
    New-Item $dbPackageDir -type directory
}

task packageDB -depends initDBPackage {
    $rh = "$dbPackageDir\rh.exe"
    New-Item -ItemType File -Path $rh -Force
    Copy-Item -Path $roundhouseExe -Destination $rh
    Copy-Item -Path $sqlDir -Destination $dbPackageDir -Force -Recurse
}

task package -depends test, packageDB, packageWeb {
    Write-Host "Deleting production package directory - '$packageDir'" -foregroundcolor "green"
    Delete $packageDir
    New-Item $packageDir -type directory

    Copy-Item -Path $webPackageDir -Destination $packageDir -Force -Recurse
    Copy-Item -Path $dbPackageDir -Destination $packageDir -Force -Recurse

    Write-Host "Creating $slnDir\${env}Package.zip with production package subdirectories." -foregroundcolor Green
    Add-Type -assembly "system.io.compression.filesystem"
    if(Test-Path "${slnDir}\${env}_package.zip") {
        Write-Host "Deleting existing production pacakge file '${slnDir}\${env}_package.zip'." -ForegroundColor Green
        Remove-Item "${slnDir}\${env}_package.zip"
    }
    [io.compression.zipfile]::CreateFromDirectory("$packageDir", "${packageZip}")
}
