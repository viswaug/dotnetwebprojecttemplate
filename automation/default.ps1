#############################################################
#Declare default values for the properties that are applicable (which in almost all cases should be all properties)
#These properties will be overridden by property values set in local and machine specific properties files
#############################################################
$defaultPropertiesFilePath = ".\default.properties.ps1"
if(Test-Path $defaultPropertiesFilePath) {
    Write-Host "Including file $defaultPropertiesFilePath" -foregroundcolor "green"
    Include (Resolve-Path $defaultPropertiesFilePath).Path
}


##############################################################
#Include any local and machine specific properties file that are found
#These included properties will be overridden by property values passed in to the script via the commandline
##############################################################
$localPropertiesFilePath = ".\local.properties.ps1"
if(Test-Path $localPropertiesFilePath) {
    Write-Host "Including file $localPropertiesFilePath" -foregroundcolor "green"
    Include (Resolve-Path $localPropertiesFilePath).Path
}

$machinePropertiesFilePath = ".\${env}.properties.ps1"
if(Test-Path $machinePropertiesFilePath) {
    Write-Host "Including file $machinePropertiesFilePath" -foregroundcolor "green"
    Include (Resolve-Path $machinePropertiesFilePath).Path
}

##############################################################
#Include file were derived properties are calculated
##############################################################
if(Test-Path ".\derived.Properties.ps1") {
    Write-Host "Including file $((Resolve-Path ".\derived.Properties.ps1").Path)" -foregroundcolor "green"
    Include (Resolve-Path ".\derived.Properties.ps1").Path
}

###############################################################
#All tasks begin here
###############################################################
task writeAllProperties `
    -Description "Outputs all the properties used by the build script" `
{
    $props = & $psake.get_variables
    Format-Table -Wrap -AutoSize -Property @("Name", "Value") -InputObject $props | Out-string
}

task cleanNugetPackages `
    -Description "Deletes all the downloaded nuget packages" `
{
    Write-Host "Deleting nuget packages directory - '$nugetPackagesDir'" -foregroundcolor "green"
    Delete "$nugetPackagesDir"
    Write-Host "Deleting nuget tools directory - '$toolsNugetPackagesDir'" -foregroundcolor "green"
    Delete "$toolsNugetPackagesDir"
}

task cleanNpmModules `
    -Description "Deletes all the downloaded NPM modules" `
{
    Write-Host "Deleting node_modules directory - '$nodeModulesDir'" -foregroundcolor "green"
    Delete "$nodeModulesDir"
}

task cleanBuildOutput `
    -Description "Deletes all the output produced by the build, package, test tasks. Does not clean the nuget packages and the node modules" `
    -depends writeAllProperties `
{
    Write-Host "Cleaning build output directory - '$buildOutputDir'" -foregroundcolor "green"
    Delete $buildOutputDir
    Delete $webOutputDir
    Delete $testResultsDir
    Delete $dbPackageDir
    Delete $packageDir
    Delete $packageZip
    Delete $webPackageDir
}

task clean `
    -description "same as the cleanBuildOuput task" `
    -depends cleanBuildOutput { }

task cleanAll `
    -description "Deletes downloaded nuget packages, node modules, build output, test results and web output." `
    -depends cleanNugetPackages, cleanNpmModules, clean { }

task bootstrapNuget `
    -description "Downloads nuget to temporary location on machine if not already present" `
{
    if(-Not (Test-Path $nugetExe)) {
        if(-Not (Test-Path $nugetExePath)) {
            New-Item $nugetExePath -ItemType Directory
        }
        Write-Host "Downloading Nuget from '$nugetDownloadUrl' to '$nugetExe'" -ForegroundColor Green
        Invoke-WebRequest $nugetDownloadUrl -OutFile $nugetExe
        Write-Host "Done downloading Nuget" -ForegroundColor Green
    }
    $env:Path = $nugetExePath + ";" + $env:Path
    Write-Host "Adding Nuget to path" -ForegroundColor Green
    Write-Host "Nuget version is $nugetVersion" -ForegroundColor Green
}

task bootstrapNode `
    -description "Downloads node and npm to temporary location on machine if not already present" `
{
    if(-Not (Test-Path $nodeZipDownloadFile)) {
        Write-Host "Downloading Node from '$nodeZipDownloadUrl' to '$nodeZipDownloadFile'" -ForegroundColor Green
        Invoke-WebRequest $nodeZipDownloadUrl -OutFile $nodeZipDownloadFile
        Write-Host "Done downloading Node" -ForegroundColor Green
    }
    if(-Not (Test-Path $nodeExe)) {
        Add-Type -AssemblyName System.IO.Compression.FileSystem
        Write-Host "Extracting '$nodeZipDownloadFile' to '$nodeDownloadPath'" -ForegroundColor Green
        [System.IO.Compression.ZipFile]::ExtractToDirectory($nodeZipDownloadFile, $nodeDownloadPath)
        Write-Host "Done extracting Node" -ForegroundColor Green
    }
    $env:Path = $nodeExePath + ";" + $env:Path
    Write-Host "Adding Node to path" -ForegroundColor Green
    Write-Host "Node version is " (& node --version) -ForegroundColor Green
}

task initNugetPackages `
    -description "Downloads nuget packages" `
    -depends bootstrapNuget `
{
    Write-Host "Running nuget packages install" -foregroundcolor "green"
    exec {& (Join-Path $nugetDir "installPackages.ps1")}
    Write-Host "Running nuget tools install" -foregroundcolor "green"
    exec {& (Join-Path $nugetDir "installTools.ps1")}
}

task resetDB `
    -description "Drops the existing database and recreates it from the DB scripts." `
{
    Write-Host "Dropping database" -foregroundcolor "green"
    exec {& $roundhouseExe --silent --drop --connstring $dbSetupConnectionString}
    Invoke-Task initDB
}

task initDB `
    -description "Creates the database if it does not already exist. Runs the database migration scripts against the database" `
    -precondition { $runInitDB } `
{
    Write-Host "Running database migrations" -foregroundcolor "green"
    exec {& $roundhouseExe --silent --connstring $dbSetupConnectionString --files $sqlDir --runallanytimescripts --ra $everytimeScriptsDir}
}

task init `
    -description "Cleans the build output and runs database migrations" `
    -depends clean, initDB `
{
    New-Item $buildOutputDir -type directory
    New-Item $testResultsDir -type directory
}

task config `
    -description "Generates files like configuration files from their corresponding template files. All files ending in '.template' will be processed by replacing property tokens in them and writing out new files without the '.template' extension. For example, a file named 'abc.config.template' will generate another called 'abc.config' with the tokens replaced." `
{
    if($debug) {Invoke-Task writeAllProperties}
    $single, $double, $grave = "'", '"', "``"
    #Replace the tokens in all template files under solution directory
    Get-ChildItem -Path $slnDir -Filter *.template -Recurse | ?{ $_.fullname -notmatch "\\node_modules\\?" } | ForEach-Object {
        Write-Host "Replacing tokens in " $_.fullName -foregroundcolor "green"
        $newFileName = $_.FullName -replace ".template$", ""
        $fileContents = [System.IO.File]::ReadAllText($_.FullName)
        $fileContents = $fileContents -replace "($single|$double|$grave)", '`$1'
        $fileContents = $fileContents -replace '\$\$', '`$'
        $fileContents = '"' + $fileContents + '"'
        $fileContents = [System.Environment]::ExpandEnvironmentVariables($fileContents)
        Invoke-Expression -Command $fileContents | Out-File $newFileName -Encoding ASCII
    }
}

task setupPath `
    -description "Adds nuget and node in the temporary location to the environment PATH" `
    -depends bootstrapNuget, bootstrapNode `
{
    $env:Path = $nodeExePath + ";" + $env:Path
    $env:Path = $nugetExePath + ";" + $env:Path
}

task initNpmPackages `
    -depends bootstrapNode `
{
    $webProjectDir = (Get-Item $webProjectFile).DirectoryName
    $pkgJson = "${webProjectDir}\package.json"
    if(Test-Path $pkgJson) {
        Push-Location $webProjectDir
        Exec { npm install}
        Pop-Location
    } else {
        Write-Host "Skipping NPM INSTALL. '$pkgJson' file was not found" -ForegroundColor Green
    }
}

task gruntBuild `
    -description "Runs the default task in the grunt file located in the root of the website's root directory" `
    -depends bootstrapNode `
{
    $webProjectDir = (Get-Item $webProjectFile).DirectoryName
    $gruntFile = "${webProjectDir}\gruntfile.js"
    if(Test-Path $gruntFile) {
        Push-Location $webProjectDir
        $tasks = $(ConvertTo-Json $gruntTasks -Compress)
        $options = $(ConvertTo-Json $gruntOptions -Compress)
        $double = '"'
        $gruntCommand = "require('grunt').tasks($tasks, $options);" -replace "($double)", '"$1'
        Exec { node -e $gruntCommand }
        Pop-Location
    } else {
        Write-Host "Skipping Grunt run. '$gruntFile' file was not found" -ForegroundColor Green
    }
}

task build `
    -description "Builds the website after generating the configuration files" `
    -depends init, compile `
{}

task compile `
    -description "Builds the website after generating the configuration files" `
    -depends config `
{
    $logParam = '/l:FileLogger,Microsoft.Build.Engine;logfile=Build_LOG.log'
    Exec { msbuild /nologo /v:$msbuildVerbosity /m:$msbuildCpuCount /p:BuildInParralel=$msbuildParralel /p:Configuration="$buildConfiguration" /t:Build /p:OutputPath="$buildOutputDir" /p:CreatePackageOnPublish=False /p:DeployOnBuild=False /p:VSToolsPath="$VSToolsDir" $logParam "$webSlnFile" }
}

task test `
    -description "Builds the website and runs units that were located. All unit test assemblies should end with '.Tests.dll'" `
    -depends build `
{
    if(Test-Path($buildOutputDir))
    {
        $testAssemblies = Get-ChildItem -Path $buildOutputDir\*.Tests.dll -Exclude *.Integration.Tests.dll,*.Selenium.Tests.dll
    }
    else
    {
        $testAssemblies = $null
    }
    $testFileNames = @()

    if(!$testAssemblies) {
        Write-Host "No unit test assemblies were found to run" -ForegroundColor Yellow
        return
    }

    Write-Host "Unit Test Assemblies found:" -foregroundcolor "green"
    Write-Host "********************************************" -foregroundcolor "green"
    if($testAssemblies -is [System.IO.FileInfo]) {
        Write-Host $testAssemblies.FullName -foregroundcolor "green"
        $testFileNames += $testAssemblies.FullName
    }
    else {
        Foreach($testAssembly in $testAssemblies) {
            Write-Host $testAssembly.FullName -foregroundcolor "green"
            $testFileNames += $testAssembly.FullName
        }
    }

    Write-Host "********************************************" -foregroundcolor "green"
    Run-Tests $testRunner $testFileNames $testResultFile $testOutputFile $testErrorOutputFile
}

task rebuild `
    -description "Initializes the nuget packages and node modules. Runs the grunt build script and builds and tests the website" `
    -depends initNugetPackages, initNpmPackages, gruntBuild, test

task default `
    -description "Calls the Test task" `
    -depends test
